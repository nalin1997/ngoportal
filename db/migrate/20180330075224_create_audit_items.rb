class CreateAuditItems < ActiveRecord::Migration[5.1]
  def change
    create_table :audit_items do |t|
      t.references :audit, foreign_key: true
      t.string :content
      t.integer :quantity
      t.float :price
      t.boolean :verified,default: false

      t.timestamps
    end
  end
end
