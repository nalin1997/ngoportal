class CreateAudits < ActiveRecord::Migration[5.1]
  def change
    create_table :audits do |t|
      t.references :user, foreign_key: true
      t.references :admin_user, foreign_key: true
      t.boolean :verified, default: false


      t.timestamps
    end
  end
end
