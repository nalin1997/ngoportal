class AddAttachmentSourceToAttachments < ActiveRecord::Migration[5.1]
  def self.up
    change_table :attachments do |t|
      t.attachment :source
    end
  end

  def self.down
    remove_attachment :attachments, :source
  end
end
