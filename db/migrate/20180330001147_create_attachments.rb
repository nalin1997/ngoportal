class CreateAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :attachments do |t|
      t.string :content
      t.string :parent_type
      t.integer :parent_id
      t.boolean :verified, default: false

      t.timestamps
    end
  end
end
