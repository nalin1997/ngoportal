require 'csv'
class AuthenticatedController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :authenticate_user!
	before_action :check_details, except: [ :create_details,:submit_create_details ]
	before_action :check_files, except: [ :create_details, :registration_files , :submit_registration_files,:submit_create_details]

	# before_action :check_verified, except: [ :create_details, :registration_files , :submit_registration_files,:submit_create_details ,:application_status]
	  def index
	  end

	  def create_details 	
	  end

	  def create_employee
	  end

	  def submit_create_employee
	  	Employee.create!(name:params[:name],contact: params[:contact], user:current_user)
	  	redirect_to '/authenticated/index'
	  end

	  def take_attendance
		
		@employees=@current_user.employees
	  end	

	  def show_attendance
	  	@attendances = Attendance.where(employee_id: current_user.employees.pluck(:id))
	  end

	  def submit_attendance
		byebug
	  	employee_ids = params[:employees]
	  	employee_ids.each do|id|
	  		Attendance.create!(employee: Employee.find(id),date: Time.now())
	  	end
	  	redirect_to "/authenticated/index"
	  end

	  def create_audit
	  	@audit_item = AuditItem.new
	  end

	  def submit_create_audit
	  	audit = Audit.create(user: current_user, admin_user: 2)
	  	for i in 0..params[:audit_items].length do
	  		audit_item = params[:audit_items][i] 
	  		audit.audit_items.create!(content: audit_item[:content],price: audit_item[:price],quantity: audit_item[:quantity])
	  	end
	  	audit.attachment.create(source: params[:file], content: params[:comment])
	  	audit.save
	  	redirect_to '/authenticated/index'
	  end

	  def registration_files
	  end

	  def submit_registration_files
	  	registration_certificate = params[:registration_certificate]
	  	previous_year_balance_sheet = params[:previous_year_balance_sheet]
	  	pan_card = params[:pan_card]
	  	registration_certificate = current_user.attachments.create(source: registration_certificate,content: "Registration Certificate.")
	  	previous_year_balance_sheet = current_user.attachments.create(source: previous_year_balance_sheet,content: "Previous Year Balance Sheet.")
	  	pan_card = current_user.attachments.create(source: pan_card,content: "PAN Card of the NGO.")
	  	certi_file = CSV.read(params[:registration_certificate].tempfile)
	  	itr_file = CSV.read(params[:previous_year_balance_sheet].tempfile)
	  	pan_card = CSV.read(params[:pan_card].tempfile)
	  	certi_file = CSV.parse(certi_file)
	  	itr_file = CSV.parse(itr_file)
	  	byebug
	  	name = itr_file[1][0]
	  	pan = itr_file[1][1] 	
	  	address = itr_file[1][2].to_s + itr_file[3][0].to_s + itr_file[3][1].to_s + itr_file[3][2].to_s
	  	byebug
	  	if address.upcase == current_user.address.upcase
	  		if name.upcase == current_user.name.upcase
	  			# if pan.upcase == pan_card.source_file_name.split(/\s|\./)[0].upcase
	  				if certi_file[5][1].upcase == current_user.registration_number.upcase
	  					current_user.verified= true
	  					current_user.physically_verified= true
	  					current_user.save!
	  				end
	  			# end	
	  		end
	  	end
	  	redirect_to "/authenticated/application_status"
	  end

	  def application_status
	  	@files = current_user.attachments
	  end

	  def check_files
	  	if current_user.attachments.empty?
	  		redirect_to "/authenticated/registration_files"
	  	end
	  end

	  def check_verified
	  	unless current_user.verified
	  		redirect_to "/authenticated/application_status"
	  	end
	  end
	  def check_details
	  	unless current_user.name
	  		redirect_to "/authenticated/create_details"
	  	end
	  end

	  def submit_create_details
		current_user.name=params["name"]
		current_user.address=params["address"]
		current_user.registration_number=params["registration_number"]
		current_user.manager=params["manager"]
		current_user.contact=params["contact"]
		current_user.staff=params["staff"]
		current_user.residents=params["residents"]
		current_user.capacity=params["capacity"]
		current_user.description=params["description"]
		current_user.		save
		redirect_to '/authenticated/index'
	  end	
end
