class Attachment < ApplicationRecord
	has_attached_file :source
	do_not_validate_attachment_file_type :source
	belongs_to :parent, :polymorphic => true
end
