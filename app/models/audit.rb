class Audit < ApplicationRecord
  belongs_to :user
  belongs_to :admin_user
  has_many :audit_items
  has_one :attachment, as: parent
end
