Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'home/index'
  get 'authenticated/index'
  get "authenticated/create_details"
  get "authenticated/status"
  get "authenticated/registration_files"
  get "authenticated/application_status"
  get "authenticated/take_attendance"
  post "authenticated/submit_attendance"
  post "authenticated/submit_registration_files"
  post "authenticated/submit_create_details"
  post "authenticated/submit_create_employee"
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "authenticated#index"
end
